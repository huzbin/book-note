

#include <type_traits>
#include <iostream>

//C++11前定义常量方法
template <typename Type>
struct GetLeftSize
{
    //1. 类内静态成员常量
    static int const value = 1;
    //2. 枚举
    enum
    {
        v = 1
    };
};

//C++11

template <typename Type>
struct GetLeftSiz : std::integral_constant<int, 1>
{
    /* data */
};

int main()
{
    //类型判定is_integral
    /*
    std::cout << "is_const:" << std::endl;
    std::cout << "int:" << std::is_const<int>::value << std::endl;                //0
    std::cout << "const int:" << std::is_const<const int>::value << std::endl;    //1
    std::cout << "const int&:" << std::is_const<const int &>::value << std::endl; //0
    std::cout << "const int*:" << std::is_const<const int *>::value << std::endl; //0
    std::cout << "int* const:" << std::is_const<int *const>::value << std::endl;  //1
    */

    //is_same判断类型是否相同
    /*
    std::cout << std::is_same<int, int>::value << std::endl;//1
    std::cout << std::is_same<int, unsigned int>::value << std::endl;//0
    std::cout << std::is_same<int, signed int>::value << std::endl;//1
    */

    //is_base_of判断是否继承
    /*
    class A
    {
    };
    class B : A
    {
    };

    class C
    {
    };
    std::cout << std::is_base_of<A, B>::value << std::endl; //1
    std::cout << std::is_base_of<B, A>::value << std::endl; //0
    std::cout << std::is_base_of<C, B>::value << std::endl; //0
    */

    //is_convertible判断前面模板参数能否作为后面的模板参数类型
    /*
    class A
    {
    };
    class B : public A
    {
    };
    class C
    {
    };
    bool b2a = std::is_convertible<B *, A *>::value;
    bool a2b = std::is_convertible<A *, B *>::value;
    bool b2c = std::is_convertible<B *, C *>::value;
    std::cout << std::boolalpha << std::endl; //0
    std::cout << b2a << std::endl;            //true
    std::cout << a2b << std::endl;            //false
    std::cout << b2c << std::endl;            //false
    */
}